import { Component } from 'angular2/core';
import { Hero } from './custom_types/hero';
import { HeroDetailComponent } from './hero-detail.component';
import { HeroService } from './services/hero.service';
import { OnInit } from 'angular2/core';
import { Router } from 'angular2/router';

@Component({
  selector: 'my-heroes',
  templateUrl: 'app/templates/heroes.component.html',
  styleUrls: ['app/styles/heroes.component.css'],
  directives: [HeroDetailComponent]
})

export class HeroesComponent implements OnInit {
  public title = 'Tour of Heroes';
  public selectedHero: Hero;
  public heroes: Hero[];

  constructor(
    private _heroService: HeroService,
    private _router: Router
  ) { }
  
  ngOnInit() {
    this.getHeroes();
  }

  getHeroes() {
    this._heroService.getHeroes().then(heroes => this.heroes = heroes);
  }

  onSelect(hero: Hero) { this.selectedHero = hero; }

  gotoDetail(){
    this._router.navigate(['HeroDetail', { id: this.selectedHero.id }]);
  }
}
