import {HEROES} from '../constants/mock-heroes';
import {Injectable} from 'angular2/core';

@Injectable()
export class HeroService {
	getHero(id: number){
		return Promise.resolve(HEROES).then(
			heroes => heroes.find(hero => hero.id == id)
		);
	}

	getHeroes(){
		return Promise.resolve(HEROES);
	}

	getHeroesSlowly(){
		return new Promise<HERO[]>( resolve =>
			setTimeout(() => resolve(HEROES), 2000);
		);
	}
}
