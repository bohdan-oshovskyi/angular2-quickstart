import { Component } from 'angular2/core';
import { HeroesComponent } from './heroes.component';
import { DashboardComponent } from './dashboard.component';
import { HeroDetailComponent } from './hero-detail.component';
import { HeroFormComponent } from './hero-form.component';
import { HeroService } from './services/hero.service';
import { RouteConfig, ROUTER_DIRECTIVES, ROUTER_PROVIDERS } from 'angular2/router';
import { HighlightDirective } from './directives/highlight.directive';

@RouteConfig([
	{
	  path: '/detail/:id',
	  name: 'HeroDetail',
	  component: HeroDetailComponent
	},
	{
	  path: '/dashboard',
	  name: 'Dashboard',
	  component: DashboardComponent,
	  useAsDefault: true
	},

  {
    path: '/heroes',
    name: 'Heroes',
    component: HeroesComponent
  },
  {
    path: '/form',
    name: 'Form',
    component: HeroFormComponent
  }
])

@Component({
  selector: 'my-app',
  template: `
  	<h1>{{title}}</h1>
  	<nav>
	  	<a [routerLink]="['Heroes']">Heroes</a>
	  	<a [routerLink]="['Dashboard']">Dashboard</a>
  	</nav>
  	<router-outlet></router-outlet>
    <div>
      <input type="radio" name="colors" (click)="color='lightgreen'">Green
      <input type="radio" name="colors" (click)="color='yellow'">Yellow
      <input type="radio" name="colors" (click)="color='cyan'">Cyan
    </div>
    <p [myHighlight]="color" [defaultColor]="'red'">Highlight me!</p>
	`
  ,
  directives: [ ROUTER_DIRECTIVES, HighlightDirective],
  providers: [ ROUTER_PROVIDERS, HeroService ],
  styleUrls: ['app/styles/app.component.css']

})

export class AppComponent {
	public title = 'Tour of Heroes))';
};
