import {Component} from 'angular2/core';
import {NgForm}    from 'angular2/common';
import { FormHero }    from './custom_types/form-hero';

@Component({
  selector: 'hero-form',
  templateUrl: 'app/templates/hero-form.component.html'
})
export class HeroFormComponent {
  active = true;
  powers = ['Really Smart', 'Super Flexible',
            'Super Hot', 'Weather Changer'];
  model = new FormHero(18, 'Dr IQ', this.powers[0], 'Chuck Overstreet');
  submitted = false;

  onSubmit() { this.submitted = true; }
  newHero() {
  	this.model = new FormHero(42, '', '');
  }
}
